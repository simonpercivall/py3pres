##### General porting info
- [Supporting Python 3](http://python3porting.com)

##### Transition libraries
- [six.py](https://github.com/benjaminp/six)
- [modernize](https://github.com/python-modernize/python-modernize)

##### Leveraging Python 3
- [mypy](http://www.mypy-lang.org/)
- [typycal](http://ethanhs.me/blog/typycal.html)

##### "What's New" docs:
- [What's new in Python 3.0](https://docs.python.org/3/whatsnew/3.0.html)
- [What's new in Python 3.1](https://docs.python.org/3/whatsnew/3.1.html)
- [What's new in Python 3.2](https://docs.python.org/3/whatsnew/3.2.html)
- [What's new in Python 3.3](https://docs.python.org/3/whatsnew/3.3.html)
- [What's new in Python 3.4](https://docs.python.org/3/whatsnew/3.4.html)
- [What's new in Python 3.5](https://docs.python.org/3/whatsnew/3.5.html)
- [What's new in Python 3.6](https://docs.python.org/3/whatsnew/3.6.html)

##### `async` stuff
- [How the heck does async/await work in Python 3.5?](https://snarky.ca/how-the-heck-does-async-await-work-in-python-3-5/)
- [API design in a post-async/await world](https://vorpus.org/blog/some-thoughts-on-asynchronous-api-design-in-a-post-asyncawait-world/)
- [Curio tutorial](https://curio.readthedocs.io/en/latest/tutorial.html)
- [Trio tutorial](https://trio.readthedocs.io/en/stable/tutorial.html)
