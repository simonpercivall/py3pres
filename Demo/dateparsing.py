from typing import (
    Optional,
    Sequence,
    Union,
)
from datetime import date, datetime

class DateParser:
    formats: Sequence[str]

    def __init__(self, formats: Sequence[str], *, is_optional=False) -> None:
        self.formats = formats
        self.is_optional = is_optional

    def parse(self, value: Union[str, float, date]) -> Optional[Union[datetime, date]]:
        if isinstance(value, date):
            return value
        for format_ in self.formats:
            val = parse_date(value, date_format=format_)
            if val is not None:
                return val
        if not self.is_optional:
            raise ValueError(f'Unable to parse {value} as date')

def parse_date(val: str, date_format: str) -> Optional[date]:
    try:
        return datetime.strptime(val, date_format).date()
    except ValueError:
        return None

parser = DateParser(['%y-%m-%d', '%Y-%m-%d'])
d: date = parser.parse('2017-09-01')
