# Python 3 Presentanion

The purpose of this presentation is to discuss the differences between
Python 2.7 and Python 3.6, and to briefly touch on how to migrate to
Python 3. The presentation is divided between Keynote slides, and
Jupyter demos.

### HowTo

To get started, run:
```
> make server
```
to create a Pipenv environment, install dependencies, and run the Jupyter server.

After this, open `Python3.key` to view the presentation, and follow the `Demo`
links to get to "semi-interactive" demos in the Jupyter environment.
