.PHONY: server clean all

export PIPENV_VENV_IN_PROJECT = 1

all: rise ipywidgets

.venv:
	pipenv --three install --dev

.venv/share/jupyter/nbextensions/rise/main.js: .venv
	pipenv run pip install "vendor/rise-5.0.0+simonp.1.tar.gz"
	pipenv run jupyter nbextension install --py --sys-prefix rise
	pipenv run jupyter nbextension enable --py --sys-prefix rise
	touch .venv/share/jupyter/nbextensions/rise/main.js

.venv/share/jupyter/nbextensions/jupyter-js-widgets/extension.js: .venv
	pipenv run jupyter nbextension enable --py --sys-prefix widgetsnbextension
	touch .venv/share/jupyter/nbextensions/jupyter-js-widgets/extension.js

rise: .venv/share/jupyter/nbextensions/rise/main.js

ipywidgets: .venv/share/jupyter/nbextensions/jupyter-js-widgets/extension.js

clean:
	rm -rf .venv

server: all
	pipenv run jupyter notebook --notebook-dir=Demo --NotebookApp.enable_mathjax=False --NotebookApp.token='' --no-browser
